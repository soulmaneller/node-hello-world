const Hapi = require('@hapi/hapi');

process.on( 'unhandleRejection', err => {
  console.log( '------------- error --------------' );
  console.log( err );
  process.exit( 1 );
});

const { SERVER_HOST, SERVER_PORT } = process.env;

const server_config = {
  host: SERVER_HOST || 'localhost',
  port: SERVER_PORT || 3000,
};

console.log( `server_config:`, server_config );

const server = Hapi.server( server_config );

server.route({
  method: 'GET',
  path: '/',
  handler: ( req, h ) => {
    console.log( `[GET] / Hello word!` );
    return 'Hello world!';
  }
});


server.start().then(() => {
  console.log( 'Server running on %s', server.info.uri );
});
